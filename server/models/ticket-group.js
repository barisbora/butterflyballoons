const mongoose = require('mongoose');

let schema = new mongoose.Schema({
  GID: {
    type: String,
    required: true,
    index:true,
    unique:true,
  },
  OID: {
    type: String,
    required: false,
    index:true,
    default: false
  },
  Date: {
    type: String,
    required: true,
    index:true
  },
  Note: {
    type: String,
    required: false,
    default:null
  },
  TicketType: {
    type: String,
    required: false,
    default:null
  },
  Charges: {
    type: Array,
    required: false,
    default: []
  },
  WillCharge: {
    type: Boolean,
    required: false,
    default:true
  },
  Online: {
    type: Boolean,
    required: false,
    default: false
  }
})

const model = mongoose.model('tickets_groups', schema)
exports.TicketGroup = model
