export const state = () => ({
  announcement: true,
  paymentIFRAME: false,
})
export const getters = {}
export const mutations = {
  openIFRAME(state) {
    state.paymentIFRAME = true
  },
  closeIFRAME(state) {
    state.paymentIFRAME = false
  },
  closeANNOUNCEMENT(state) {
    state.announcement = false
  }
}
export const actions = {}
