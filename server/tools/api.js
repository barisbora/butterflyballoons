module.exports.validation = (res, error) => {
  return res.status(422).send({
    status: false,
    code: 422,
    data: error.details
      ? error.details.map(e => {
          return {
            key: e.path.join('.'),
            label: e.context.label,
            message: e.message
          }
        })
      : error
  })
}

module.exports.error = (res, data, code = 400) => {
  return res.status(code).send({
    status: false,
    code,
    data
  })
}

module.exports.notFound = (res) => {
  return res.status(404).send({
    status: false,
    code: 404,
    data: 'Not Found'
  })
}

module.exports.success = (res, data) => {
  return res.status(200).send({
    status: true,
    code: 200,
    data
  })
}

module.exports.paginate = (res, results, map = null) => {
  return res.status(200).send({
    status: true,
    code: 200,
    data: map ? results.docs.map(map) : results.docs,
    meta: {
      total: results.total,
      limit: results.limit,
      page: results.page,
      max: results.max,
      prev: results.prev,
      next: results.next
    }
  })
}
