exports.success = (message, tickets) => {
  let items = JSON.stringify(tickets)
  return `<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Butterfly</title>
	<style type="text/css">
		body, html {
			padding: 0;
			color: #333;
			background: whitesmoke;
			display: flex;
			align-items: center;
			justify-content: center;
			min-height: 100%;
			font-size: 20px;
			font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
			padding: 20px;
			box-sizing: border-box;
		}
		.success {
			color: #279b41;
			font-weight: bold;
			text-align: center;
		}
		
		.success small {
			font-size: 15px;
			display: block;
			margin-top: 15px;
			color: #333;
		}
	</style>
</head>
<body>
<div class="success">
	<div>Payment Successful</div>
	<small>${message}</small>
</div>
<script type="text/javascript">
parent.closePaymentModal('${items}')
</script>
</body>
</html>
`
}

exports.error = (message) => {
  return `<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Butterfly</title>
	<style type="text/css">
		body, html {
			padding: 0;
			color: #333;
			background: whitesmoke;
			display: flex;
			align-items: center;
			justify-content: center;
			min-height: 100%;
			font-size: 20px;
			font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
			padding: 20px;
			box-sizing: border-box;
		}
		.error {
			color: #9b2836;
			font-weight: bold;
			text-align: center;
		}
		
		.error small {
			font-size: 15px;
			display: block;
			margin-top: 15px;
			color: #333;
		}
	</style>
</head>
<body>
<div class="error">
	<div>Payment Failed</div>
	<small>${message}</small>
</div>
</body>
</html>
`
}
