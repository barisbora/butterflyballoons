const mongoose = require('mongoose');

let schema = new mongoose.Schema({
  Date: {
    type: String,
    required: true,
    index: true
  },
  Capacities: {
    type: mongoose.Schema.Types.Mixed,
    required: true,
  },
  Online: {
    type: Boolean,
    required: false,
    default: true,
  }
})

const model = mongoose.model('capacities', schema)

exports.Capacity = model
exports.CapacityResource = (item) => {
  return {
    ID: item._id,
    Date: item.Date,
    Capacities: item.Capacities,
    Online: item.Online,
  }
}
