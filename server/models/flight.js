const mongoose = require('mongoose');
const Joi = require('@hapi/joi');

let schema = new mongoose.Schema({
  Name: {
    type: String,
    required: true,
  },
  Price: {
    type: Number,
    required: true,
  },
  Sortie: {
    type: Boolean,
    required: false,
    default: false
  },
  Duration: {
    type: Number,
    required: true,
  },
  Description: {
    type: String,
    required: false,
    default: null
  },
  Features: {
    type: Array,
    required: false,
    default: []
  },
  Transactions: [
    {
      Name: {
        type: String,
        required: true,
      },
      Description: {
        type: String,
        required: true,
      },
      Date: {
        type: Date,
        required: false,
        default: new Date().getTime()
      }
    }
  ],
  UpdatedAt: {
    type: Date,
    required: false
  },
  CreatedAt: {
    type: Date,
    required: false,
    default: new Date().getTime()
  },
  IsDeleted: {
    type: Boolean,
    required: false,
    default: false
  }
})

const model = mongoose.model('flights', schema)

function validate(data) {
  return Joi.object({
    Name: Joi.string().label('hizmet adı').min(1).max(50).required(),
    Price: Joi.number().label('hizmet ücreti').min(0).required(),
    Sortie: Joi.boolean().label('sorti').default(false).required(),
    Duration: Joi.number().label('uçuş süresi').min(0).required(),
    Description: Joi.string().label('açıklama').empty('').allow(null).default(null).optional(),
  }).validate(data);
}

exports.Flight = model
exports.validate = validate;
exports.FlightResource = (item) => {
  return {
    ID: item._id,
    Name: item.Name,
    Price: item.Price,
    Sortie: item.Sortie,
    Duration: item.Duration,
    Description: item.Description,
    Transactions: item.Transactions,
    UpdatedAt: item.UpdatedAt,
    CreatedAt: item.CreatedAt,
    IsDeleted: item.IsDeleted,
  }
}
