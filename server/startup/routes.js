const express = require('express');
const buy = require('../routes/buy');

module.exports = function(app) {
  app.use(express.json({limit: '50mb'}));
  app.use('/api/buy', buy);
}
