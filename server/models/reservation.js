const mongoose = require('mongoose');

function randomString(length) {
  const chars = '123456789ABCDEFGHJKLMNPQRSTUVWXYZ'
  var result = '';
  for (var i = length; i > 0; --i) result += chars[Math.floor(Math.random() * chars.length)];
  return result;
}

let schema = new mongoose.Schema({
  ID: {
    type: mongoose.Schema.Types.String,
    required: true,
    index:true,
    unique:true,
  },
  OrderID: {
    type: mongoose.Schema.Types.String,
    required: false,
    index:true,
    default: null
  },
  OnlineID: {
    type: mongoose.Schema.Types.String,
    required: false,
    index:true,
  },
  WaitingPayment: {
    type: mongoose.Schema.Types.Boolean,
    required: false,
    index: true,
    default: false
  },
  Pickup: {
    type: mongoose.Schema.Types.Boolean,
    required: false,
    index: true,
    default: true
  },
  Agency: {
    ID: {
      index: true,
      required: false,
      type: mongoose.Schema.Types.String,
      default: null
    },
    Fixed: {
      required: false,
      type: mongoose.Schema.Types.Mixed,
      default: null
    },
    Rate: {
      required: false,
      type: mongoose.Schema.Types.Mixed,
      default: null,
    },
    Single: {
      required: false,
      type: mongoose.Schema.Types.Boolean,
      default: true,
    }
  },
  CreateDate: {
    type: mongoose.Schema.Types.String,
    required: true,
    index:true
  },
  ReservationDate: {
    type: mongoose.Schema.Types.String,
    required: true,
    index:true
  },
  ReservationTypeID: {
    type: mongoose.Schema.Types.String,
    required: true,
    index:true
  },
  Description: {
    type: mongoose.Schema.Types.String,
    required: false,
    default: null
  },
  Summary: {
    Chest: {
      Debt: {
        type: mongoose.Schema.Types.Number,
        required: true,
        default: null
      },
      Credit: {
        type: mongoose.Schema.Types.Number,
        required: true,
        default: null
      }
    },
    Total: {
      Debt: {
        type: mongoose.Schema.Types.Number,
        required: true,
        default: null
      },
      Credit: {
        type: mongoose.Schema.Types.Number,
        required: true,
        default: null
      }
    }
  },
  Tickets: [
    {
      TID: {
        type: mongoose.Schema.Types.String,
        required: true,
        index: true
      },
      BalloonID: {
        type: mongoose.Schema.Types.ObjectId,
        required: false,
        index: true,
        default: null
      },
      ServiceID: {
        type: mongoose.Schema.Types.ObjectId,
        required: false,
        index: true,
        default: null
      },
      Compartment: {
        type: mongoose.Schema.Types.String,
        required: false,
        default: null
      },
      Name: {
        type: mongoose.Schema.Types.String,
        required: false,
        default: "",
        index:true
      },
      Age: {
        type: mongoose.Schema.Types.Number,
        required: false,
        default: "",
      },
      Email: {
        type: mongoose.Schema.Types.String,
        required: false,
        default: "",
      },
      Gender: {
        type: mongoose.Schema.Types.String,
        required: false,
        default: "",
      },
      HotelName: {
        type: mongoose.Schema.Types.String,
        required: false,
        default: "",
      },
      HotelRoom: {
        type: mongoose.Schema.Types.String,
        required: false,
        default: "",
      },
      Nation: {
        type: mongoose.Schema.Types.String,
        required: false,
        default: "",
      },
      Passport: {
        type: mongoose.Schema.Types.String,
        required: false,
        default: "",
      },
      Phone: {
        type: mongoose.Schema.Types.String,
        required: false,
        default: "",
      },
      ServiceTime: {
        type: mongoose.Schema.Types.String,
        required: false,
        default: "",
      },
      InformPickUp: {
        type: mongoose.Schema.Types.Boolean,
        required: false,
        default: false,
      },
      InformCall: {
        type: mongoose.Schema.Types.Boolean,
        required: false,
        default: false,
      }
    }
  ],
  Transactions: [
    {
      ID: {
        type: mongoose.Schema.Types.String,
        required: true,
        index: true
      },
      Currencies: {
        type: mongoose.Schema.Types.Mixed,
        required: false,
      },
      Currency: {
        type: mongoose.Schema.Types.String,
        required: true
      },
      CurrencyDate: {
        type: mongoose.Schema.Types.String,
        required: true
      },
      Date: {
        type: mongoose.Schema.Types.String,
        required: true
      },
      Description: {
        type: mongoose.Schema.Types.String,
        required: false,
        default: ""
      },
      Price: {
        type: mongoose.Schema.Types.Number,
        required: true
      },
      ReservationTypeID: {
        type: mongoose.Schema.Types.String,
        required: false
      },
      TicketID: {
        type: mongoose.Schema.Types.String,
        required: false
      },
      Type: {
        type: mongoose.Schema.Types.String,
        required: true,
        index: true
      }
    }
  ],
  Commission: {
    type: mongoose.Schema.Types.Number,
    required: true,
  },
  IsOnline: {
    type: mongoose.Schema.Types.Boolean,
    required: false,
    default: false
  },
  CreatedBy: {
    type: mongoose.Schema.Types.Mixed,
    required: true,
  },
  ClosedBy: {
    type: mongoose.Schema.Types.Mixed,
    required: false,
    default: null
  },
  CancelBy: {
    type: mongoose.Schema.Types.Mixed,
    required: false,
    default: null
  },
  CancelDate: {
    type: mongoose.Schema.Types.String,
    required: false,
    default: null
  },
})

exports.OnlineKey = () => {

  return new Promise((resolve, reject) => {

    const key = randomString(7);

    model.findOne({OnlineID: key}, {_id:1}).then(isFound => {
      if (isFound === null) {
        return resolve(key)
      }

      return this.random()
    })

  })

}

const model = mongoose.model('reservations', schema, 'reservations')
exports.Reservation = model
