import Swal from 'sweetalert2'
export default function({ $axios, store, app }) {

  $axios.onError(err => {

    if (err.response.status === 400) {

      Swal.fire('', err.response.data.data, 'error');

      return Promise.reject(err.response.data.data)

    }

    return Promise.reject(err)
  })
}
